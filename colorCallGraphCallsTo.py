#Colors all calls along the call graph to the current address
#@author 
#@category
#@keybinding 
#@menupath 
#@toolbar 

from ghidra.program.model.address import Address
from ghidra.program.model.address import AddressSet
from java.awt import Color

def collect_calls(addresses, address):
	f = getFunctionContaining(address)
	if f == None :
		return
	print f.getName()
	e = f.getEntryPoint()
	for r in getReferencesTo(e):
		if r.getReferenceType().isCall() :
				a = r.getFromAddress()
				addresses.add(a)
				collect_calls(addresses, a)

addresses = AddressSet()

collect_calls(addresses, currentAddress)

colors = {
    "RED" : Color.RED,
    "BLACK" : Color.BLACK,
    "BLUE" : Color.BLUE,
    "CYAN" : Color.CYAN,
    "DARK_GRAY" : Color.DARK_GRAY,
    "GRAY" : Color.GRAY,
    "GREEN" : Color.GREEN,
    "LIGHT_GRAY" : Color.LIGHT_GRAY,
    "MAGENTA" : Color.MAGENTA,
    "ORANGE" : Color.ORANGE,
    "PINK" : Color.PINK,
    "RED" : Color.RED,
    "YELLOW" : Color.YELLOW
}

color = askChoice('Select color',
                          'Select the color to mark function calls',
                          colors.keys(),
                          colors.keys()[0])

setBackgroundColor(addresses, colors[color])
