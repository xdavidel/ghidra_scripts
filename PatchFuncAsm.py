# Export function assembly instructions
#@author David Delarosa (xdavidel)
#@category Functions

from utils import utils
import ghidra.app.plugin.assembler.Assembler
import ghidra.app.plugin.assembler.Assemblers

file_obj = askFile("Choose asm file", "Choose")
if not file_obj:
    exit()

data = open(file_obj.getAbsolutePath(), "rb").read()
length = len(data)

# Find candidates
fm = currentProgram.getFunctionManager()
candidates_funcs = [f for f in fm.getFunctions(True) if f.getBody().getNumAddresses() >= length]

print("Looking for functions with at least %d bytes" % (length))
# for f in candidates_funcs:
#     print(f.getName() + " - " + str(f.getBody().getNumAddresses()))
selected_function = askChoice("Choose a function to overwrite", "Choose", candidates_funcs, None)
address = selected_function.getEntryPoint()
print("Patching function %s at 0x%x" % (selected_function.getName(), address.getOffset()))

# Get Assembler
asm = ghidra.app.plugin.assembler.Assemblers.getAssembler(currentProgram)

asm.patchProgram(data, address)
