# Export function assembly instructions
#@author David Delarosa (xdavidel)
#@category Functions

from utils import utils
from ghidra.program.model.symbol import RefType,SymbolType

code_manager = currentProgram.getCodeManager()
function_manager = currentProgram.getFunctionManager()
function = utils.get_function(function_manager, currentLocation.address)

if function is None:
    print 'Current selection is not a function.'
    exit()


instructions = utils.get_instruction_list(code_manager, function)

dir_object = askDirectory("Choose output Directory", "Select Directory")

# with open(dir_object.getAbsolutePath() + "/" + str(function) + ".asm", "w") as f_out:
#     for instruction in instructions:
#         f_out.write(str(instruction) + "\n")

with open(dir_object.getAbsolutePath() + "/" + str(function) + ".asm", "wb") as f_out:
    for instruction in instructions:
        f_out.write(str(instruction.getBytes().tostring()))