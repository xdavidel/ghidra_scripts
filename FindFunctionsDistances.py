# This script find the distance between any functions pairs.
#@author: David Delarosa (xdavidel)
#@category Functions

import sys

MAX_PAIRS = 50

# python version compatability
if sys.version_info >= (3, 0):
    def m_range(start, stop, step=1):
        # pylint: disable=no-member
        return range(start, stop, step)
else:
    def m_range(start, stop, step=1):
        return xrange(start, stop, step)

class FuncPair:
    def __init__(self,f,g,dist):
        self.f = f
        self.g = g
        self.dist = dist
    def __str__(self):
        return hex(self.dist) + " : " + self.f + " - " + self.g

def write_buffer_to_file(ghidra_file, buffer):
    with open(ghidra_file.getAbsolutePath() + "/pairs.txt", "a") as pairs_f:
        for pair in buffer:
            pairs_f.write(pair.__str__() + "\n")

# Set distance limit
limit_distance = askLong("Choose Max Distance", "Enter distance (0 for any)")
if limit_distance <= 0:
    limit_distance = currentProgram.getMaxAddress().getOffset()
print("Searching for pair with distance less than 0x%x" % (limit_distance))

# Get functions from Function Manager
fm = currentProgram.getFunctionManager()
functions = [f for f in fm.getFunctions(True)]

# Calculate the number of functions
length = len(functions)
dir_object = None

if length ** 2 > MAX_PAIRS:
    print("More than %d pairs, outputing to a file..." %(MAX_PAIRS))
    dir_object = askDirectory("Choose output Directory", "Select Directory")

pairs = []

# Loop over all combinations
for i in m_range(0, length):
    for j in m_range(i, length):
        f = functions[i]
        g = functions[j]

        # safeguard for same function
        if f.getName() == g.getName():
            continue

        distance = f.getEntryPoint().subtract(g.getEntryPoint())
        abs_distance = distance if distance > 0 else distance * -1

        # Register only limited distances
        if abs_distance <= limit_distance:
            if distance > 0:
                pairs.append(FuncPair(f.getName(), g.getName(), abs_distance))
            else:
                pairs.append(FuncPair(g.getName(), f.getName(), abs_distance))

        # write caches pairs
        if dir_object and len(pairs) > MAX_PAIRS:
            write_buffer_to_file(dir_object, pairs)
            del pairs [:]

# write remaning pairs
    if dir_object:
        write_buffer_to_file(dir_object, pairs)
        del pairs [:]

if dir_object is None:
    pairs.sort()
    # Print a sorted pair list by offsets
    print(pairs)

print("Done.")
