# Binwalk.py

Runs binwalk on the current program and bookmarks the findings.

## Dependencies

- Requires binwalk to be in $PATH.

![Binwalk](./img/binwalk.png)
