# BasicBlocks.py

This Ghidra script allows you to print the basic static blocks.

## Disclaimer

The current algorithm is inefficient and can take LOTS of time on large binaries.
