# FindBannedFunctions.py

This Ghidra script display potentially dangerous functions that could introduce a vulnerability if they are used incorrectly.

The script includes a list of functions from Microsoft SDL.
