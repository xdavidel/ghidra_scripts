# Patching

A collection of patching scripts for Ghidra

## Utilities

- [Patching](#patching)
  - [Utilities](#utilities)
    - [ExtractFuncAsm.py](#extractfuncasmpy)
    - [PatchFuncAsm.py](#patchfuncasmpy)
    - [PatchFromAsm.py](#patchfromasmpy)
    - [SavePatch.py](#savepatchpy)
  - [Credits](#credits)

### ExtractFuncAsm.py

Extract function assembly to a file.

### PatchFuncAsm.py

Patching a function from a file containing assembly data.

### PatchFromAsm.py

Patching the program from an assembly file, starting from the current address to the end of the assembly file data.

### SavePatch.py

This ghidra script writes small modifications made in a executable back to the analysed file.
This allows you to edit a binary and save the modifications, without having to reload the file in raw mode.
The script was developed for ELF and PE files, but *might* work with other file types as well.

* Make the change to the executable
* (optional) Select the patched lines in the listing window (not just highlight; **select**)
* Run the SavePatch script
* If you did not select the patched area, you will be asked for its start address and lenght
* Select a location for the changed file. If the file does not exist, the current executable is copied there and then modified. Otherwise, the existing file is patched.
* Check the results!

Once you have saved the first patched location, you can select another location, re-run the script and chose the same target file.
The script will open the file again and apply the second patch.
These steps can be repeated multiple times.

## Credits

Thanks to [schlafwandler](https://github.com/schlafwandler) for his `SavePatch.py` script.